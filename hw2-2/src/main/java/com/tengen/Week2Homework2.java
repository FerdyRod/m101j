package com.tengen;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.QueryBuilder;
import java.net.UnknownHostException;
import org.bson.types.ObjectId;

/**
 * Created by ferdy.rodri on 01-15-14.
 */
public class Week2Homework2 {
    public static void main(String[] args) throws UnknownHostException {
        MongoClient client = new MongoClient();
        DB db = client.getDB("students");
        DBCollection collection = db.getCollection("grades");

        DBObject query = QueryBuilder.start("type").is("homework").get();

        DBCursor cursor = collection.find(query).sort(new BasicDBObject("student_id", 1)
                .append("score", -1));

        Integer prev_id = -1;
        ObjectId id;

        try {
            while(cursor.hasNext()){
                DBObject obj = cursor.next();
                Integer student_id = (Integer) obj.get("student_id");
                if (prev_id.equals(student_id)){
                    id = (ObjectId) obj.get("_id");
                    collection.remove(new BasicDBObject("_id", id));
                }
                prev_id = student_id;
            }
        } finally {
            cursor.close();
        }
    }

}
