package com.tengen;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import java.net.UnknownHostException;
import java.util.ArrayList;
import org.bson.types.ObjectId;

/**
 * Created by ferdy.rodri on 01-15-14.
 */
public class Week3Homework1 {
    public static void main(String[] args) throws UnknownHostException {
        MongoClient client = new MongoClient();
        DB db = client.getDB("school");
        DBCollection collection = db.getCollection("students");
        BasicDBObject query = new BasicDBObject();
        DBCursor cursor = collection.find(query);
        try {
            while(cursor.hasNext()){
                DBObject lowScore = null;
                DBObject obj = cursor.next();
                ArrayList<BasicDBObject> scores = (ArrayList) obj.get("scores");
                for (BasicDBObject score : scores) {
                    if (score.get("type").equals("homework")){
                        if (lowScore == null){
                            lowScore = score;
                        }

                        Double newScore = Double.parseDouble( score.get("score").toString());
                        Double oldScore = Double.parseDouble( lowScore.get("score").toString());

                        if (Double.compare(oldScore, newScore) > 0) {
                            lowScore = score;
                        }
                    }
                }
                scores.remove(lowScore);
                obj.put("scores", scores);
                collection.update( new BasicDBObject("_id",obj.get("_id")), obj, true, false );
            }
        } finally {
            cursor.close();
        }
    }

}
